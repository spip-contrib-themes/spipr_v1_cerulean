<?php
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(
	// T
	'theme_bscerulean_description' => 'A calm, blue sky',
	'theme_bscerulean_slogan' => 'A calm, blue sky',
);
